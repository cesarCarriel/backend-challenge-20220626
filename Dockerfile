FROM python:3.8.10

WORKDIR /code

COPY Pipfile Pipfile.lock /code/
RUN pip install pipenv && pipenv install --system --deploy

COPY . /code/

WORKDIR /code/src/