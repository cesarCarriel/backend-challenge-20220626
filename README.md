# [Challenge 20220626](https://lab.coodesh.com/cesarcarrieldev/challenge-20220626)

---

## Fitness Foods LC

### Problema:

A Fitness Foods LC precisa de uma solução que busca dados de produtos da [Open Food Facts](https://world.openfoodfacts.org/)
consolidando-os em um banco de dados, o sistema deve fazer busca frequentes para sempre manter a base atualizada. Para acessar
esses dados de forma rápida deve ser criado uma API para acesso de todos os produtos.

---

### Solução:

Para resolver o problema criei um sistema para scraping de dados da [Open Food Facts](https://world.openfoodfacts.org/) , após a captura os dados são
tratados e formados para serem salvos em uma base de dados o processo ocorre a cada 2 horas. Caso ocorra algum problema
é gerado um log de erro na base de dados onde pode ser consultada postoriormente para análise o incidente. Para
facilitar o acesso dessa informação implementei um painel administrativo para gerenciamento dos produtos capturados e
para implementei também uma API Rest para acesso desses dados paginados, além de uma documentação para API
com o [Swagger](https://swagger.io/) e o [Redoc](https://github.com/Redocly/redoc).

Para agilizar o teste a sincronização dos dados já se inicia após a inicialização da aplicação, apenas espere alguns
segundos que já será possivel visualizar os produtos na API e painel administrativo.

---

### Tecnologias:
- **Python**: Linguagem de programação de alto nível, amplamente utilizada e conhecida por sua legibilidade e sintaxe clara.
- **Django**: Framework web em Python que permite o desenvolvimento rápido e seguro de aplicativos web e criação de interfaces
adminstrativas de maneira muito fácil.
- **Django Rest Framework**: Extensão do Django que simplifica a criação de APIs RESTful.
- **Requests**: Biblioteca Python para fazer requisições HTTP de forma fácil.
- **BeautifulSoup**: Biblioteca Python para análise e extração de dados de HTML e XML.
- **PostgreSQL**: Sistema de gerenciamento de banco de dados relacional de código aberto, confiável e escalável.

---

### Arquitetura:

A arquitetura do projeto segue os princípios do Domain-Driven Design (DDD) e da Clean Architecture, promovendo uma 
separação clara de responsabilidades e a modularidade do código.

A estrutura do projeto é dividida em:

- **Domain**: Camada que encapsula as entidades, objetos de valor e regras de negócio centrais da aplicação.
- **Application**: Camada que contém a lógica de negócio e coordena as interações entre as camadas internas e externas.
- **Infrastructure**: Camada que lida com os detalhes técnicos e as tecnologias externas necessárias para suportar a execução da aplicação.
- **Presentation**: Camada responsável pela interação do usuário com a aplicação.

Essa arquitetura proporciona uma separação clara de preocupações, facilita a manutenção e evolução do código, e 
permite o desenvolvimento e teste independente de cada camada.

---

### Como utilizar:

#### Docker e docker-compose:

A aplicação tem um arquivo `start.sh` que reune os comandos para inicialização.

Para executar esse arquivo é preciso conceder a permissão de execução com o seguinte comando:

```bash
sudo chmod +x start.sh
```

E agora para executa utilize o seguinte comando:

```bash
./start.sh
```

O comando acima inicia aplicação, cria as tabelas no banco de dados e cria um usuário para acessar o painel
administrativo.

Para criar um usuário para o painel administrativo utilize o seguinte comando:

```bash
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('coodesh', 'coodesh@coodesh.com.br', 'coodesh')" | docker exec -i django python manage.py shell
```

O usuário e senha do painel administrativo são `coodesh`.

### Acessos:

- Swagger: [localhost/swagger/](localhost/swagger/)
- Redoc: [localhost/redoc/](localhost/redoc/)
- Painel administrativo: [localhost/admin/](localhost/admin/)

>  This is a challenge by [Coodesh](https://coodesh.com/)