from abc import abstractmethod, ABC


class OpenFoodFactsGateway(ABC):
    @abstractmethod
    def get_products_with_detail(self):
        raise NotImplemented
