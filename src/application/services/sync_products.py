from typing import Type

from application.gateways import OpenFoodFactsGateway
from domain.log.models import ErrorLog
from domain.product.choices import ProductStatusChoices
from domain.product.models import Product


class SyncProductService:
    def __init__(self, open_food_facts_gateway: Type[OpenFoodFactsGateway]):
        self.open_food_facts_gateway = open_food_facts_gateway()

    def execute(self):
        try:
            for product in self.open_food_facts_gateway.get_products_with_detail():
                Product.objects.update_or_create(
                    url=product['url'],
                    code=product['code'],
                    defaults=dict(
                        barcode=product['barcode'],
                        status=ProductStatusChoices.imported,
                        name=product['name'],
                        quantity=product['quantity'],
                        categories=product['categories'],
                        packaging=product['packaging'],
                        brands=product['brands'],
                        image_url=product['image_url']
                    )
                )

        except Exception as e:
            ErrorLog.objects.create_product_sync_error(description=e)
