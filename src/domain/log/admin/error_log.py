from django.contrib import admin

from domain.log.models import ErrorLog


@admin.register(ErrorLog)
class ErrorLogAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'created_at')
    fields = ('id', 'name', 'description', 'created_at')
    readonly_fields = ('id', 'name', 'description', 'created_at',)
    search_fields = ('name', 'description')
    date_hierarchy = 'created_at'

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
