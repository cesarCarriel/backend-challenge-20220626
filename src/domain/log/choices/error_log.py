from django.db import models


class ErrorLogName(models.TextChoices):
    PRODUCT_SYNC_ERROR = 'PRODUCT_SYNC_ERROR', 'Erro na sincronização dos produtos'
