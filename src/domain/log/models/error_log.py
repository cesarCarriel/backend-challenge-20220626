from django.db import models

from domain.log.choices import ErrorLogName


class ErrorLogManager(models.Manager):
    def create_product_sync_error(self, **kwargs):
        self.create(**kwargs, name=ErrorLogName.PRODUCT_SYNC_ERROR)


class ErrorLog(models.Model):
    name = models.CharField('nome', max_length=20, choices=ErrorLogName.choices)
    description = models.TextField('descrição')
    created_at = models.DateTimeField('criado em', auto_now_add=True, editable=False)

    objects = ErrorLogManager()

    class Meta:
        verbose_name = 'log de erro'
        verbose_name_plural = 'logs de erros'
        db_table = 'error_log'

    def __str__(self):
        return self.name
