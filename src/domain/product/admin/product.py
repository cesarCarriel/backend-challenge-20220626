from django.contrib import admin
from django.utils.html import format_html

from domain.product.models import Product


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name', 'imported_at', 'url__display')
    fields = ('id', 'name', ('code', 'barcode'), 'status', 'url', 'quantity', 'categories', 'packaging', 'brands',
              'image_url', 'imported_at')
    search_fields = ('name', 'code')
    date_hierarchy = 'imported_at'

    @admin.display(description='url')
    def url__display(self, obj):
        return format_html('<a href="{}" target="_blank">{}</p>', obj.url, 'Visualizar')

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
