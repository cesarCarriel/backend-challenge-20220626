from django.db import models


class ProductStatusChoices(models.TextChoices):
    draft = 'draft'
    imported = 'imported'
