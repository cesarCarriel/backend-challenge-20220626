from django.db import models

from domain.product.choices import ProductStatusChoices


class Product(models.Model):
    code = models.CharField('código', max_length=100)
    barcode = models.CharField('código de barra', max_length=100, null=True, blank=True)
    status = models.CharField(max_length=10, choices=ProductStatusChoices.choices)
    imported_at = models.DateTimeField('importado em', auto_now_add=True)
    url = models.URLField(max_length=2048, unique=True)
    name = models.CharField('nome', max_length=200)
    quantity = models.CharField('quantidade', max_length=50, null=True, blank=True)
    categories = models.TextField('categorias', max_length=250, null=True, blank=True)
    packaging = models.TextField('embalagem', null=True, blank=True)
    brands = models.CharField('marcas', max_length=250, null=True, blank=True)
    image_url = models.CharField('URL da imagem', max_length=2048, null=True)

    class Meta:
        db_table = 'product'
        verbose_name = 'produto'

    def __str__(self):
        return f'{self.code} - {self.name}'
