import unicodedata

import requests
from bs4 import BeautifulSoup


class SiteAccessException(Exception):
    def __init__(self, status_code: int):
        self.mensagem = f'Erro ao acessar o site, status code: {status_code}'


class OpenFoodFactsCrawlerClient:
    @staticmethod
    def __formatted_string(value: str):
        return unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('utf-8')

    def get_products(self):
        base_url = 'https://world.openfoodfacts.org'
        response = requests.get(base_url)

        if response.status_code == 200:
            soup = BeautifulSoup(response.content, "html.parser")

            products_reponse = soup.css.select(".products li")
            products = []

            for product_reponse in products_reponse:
                product_path = product_reponse.find('a').attrs['href']
                url = base_url + product_path

                name = self.__formatted_string(value=product_reponse.css.select_one('a > span').text)

                code = url.split('/')[-2]

                image_url = product_reponse.find('img')

                if image_url:
                    image_url = image_url.attrs['src']

                product = dict(url=url, name=name, code=code, image_url=image_url)
                products.append(product)

            return products

        else:
            raise SiteAccessException(status_code=response.status_code)

    def get_product_detail(self, url: str):
        response = requests.get(url)

        if response.status_code == 200:
            soup = BeautifulSoup(response.content, "html.parser")

            field_packaging_value = soup.css.select_one('#field_packaging_value')
            packaging = field_packaging_value.text if field_packaging_value else None

            name = self.__formatted_string(value=soup.find('h1', property='food:name').text)

            image_url = soup.css.select_one('#og_image')

            if image_url:
                image_url = image_url.attrs['src']

            barcode = soup.css.select_one('#barcode')
            if barcode:
                barcode = barcode.text

            quantity = soup.css.select_one('#field_quantity_value')
            if quantity:
                quantity = quantity.text

            categories = soup.css.select_one('#field_categories_value')
            if categories:
                categories = categories.text

            brands = soup.css.select_one('#field_brands_value')
            if brands:
                brands = brands.text

            product = dict(
                barcode=barcode,
                url=response.url,
                name=name,
                code=int(response.url.split('/')[-2]),
                quantity=quantity,
                categories=categories,
                packaging=packaging,
                brands=brands,
                image_url=image_url
            )

            return product

        else:
            raise SiteAccessException(status_code=response.status_code)
