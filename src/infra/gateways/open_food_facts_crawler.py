from application.gateways import OpenFoodFactsGateway
from infra.clients import OpenFoodFactsCrawlerClient


class OpenFoodFactsCrawlerGateway(OpenFoodFactsGateway):
    def __init__(self, open_food_facts_client=OpenFoodFactsCrawlerClient):
        self.open_food_facts_client = open_food_facts_client()

    def get_products_with_detail(self):
        products_base = self.open_food_facts_client.get_products()

        for product_base in products_base:
            product = self.open_food_facts_client.get_product_detail(url=product_base['url'])
            yield product
