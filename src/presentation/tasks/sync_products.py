from application.services.sync_products import SyncProductService
from base.celery import app
from infra.gateways import OpenFoodFactsCrawlerGateway


@app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(7200.0, sync_products.s())


@app.task
def sync_products():
    SyncProductService(open_food_facts_gateway=OpenFoodFactsCrawlerGateway).execute()
