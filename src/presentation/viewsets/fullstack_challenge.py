from rest_framework.response import Response
from rest_framework.views import APIView


class FullstackChallengeView(APIView):
    def get(self, request):
        return Response('Fullstack Challenge 20201026', status=200)
