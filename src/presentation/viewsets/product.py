from rest_framework import viewsets

from domain.product.models import Product
from presentation.serializers import ProductListSerializer, ProductDetailSerializer


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Product.objects.all()
    lookup_field = 'code'

    def get_serializer_class(self):
        if self.action == 'list':
            return ProductListSerializer

        if self.action == 'retrieve':
            return ProductDetailSerializer
